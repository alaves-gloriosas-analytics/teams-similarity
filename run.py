import pandas as pd
import warnings
from statsbombpy import sb
from processing.metrics import Metrics
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.cluster import KMeans
from icecream import ic

ic.configureOutput(includeContext=True)
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

DEBUG = True

columns_ = [
    'team',
    'goals', 'penalty_goal', 'penalty_attempted', 'penalty_conceded', 'shots', 'shots_out_box',
    'shots_in_box', 'shots_on_target',
    'yellow_cards', 'red_cards', 'second_yellow_cards',
    'tackles', 'tackles_won', 'perc_tackles_won', 'tackles_z1', 'tackles_z2', 'tackles_z3', 'tackles_z4',
    'interceptions', 'interceptions_ok', 'perc_interceptions_ok', 'interceptions_tackles', 'clearances',
    'aerial_clearances',
    'fouls_commited', 'fouls_received', 'lost_ball_recoveries',
    'aerial_duels_won', 'aerial_duels_lost', 'perc_aerial_duels_won',
    'dribbles_completed', 'dribbles_attempted', 'perc_dribbles_completed', 'dribbles_completed_z1',
    'dribbles_attempted_z1', 'perc_dribbles_completed_z1', 'dribbles_completed_z2', 'dribbles_attempted_z2',
    'perc_dribbles_completed_z2', 'dribbles_completed_z3', 'dribbles_attempted_z3', 'perc_dribbles_completed_z3',
    'dribbles_completed_z4', 'dribbles_attempted_z4', 'perc_dribbles_completed_z4',
    'passes_completed', 'passes_attempted', 'perc_passes_completed', 'passes_completed_z1', 'passes_attempted_z1',
    'perc_passes_completed_z1', 'passes_completed_z2', 'passes_attempted_z2', 'perc_passes_completed_z2',
    'passes_completed_z3', 'passes_attempted_z3', 'perc_passes_completed_z3', 'passes_completed_z4',
    'passes_attempted_z4', 'perc_passes_completed_z4', 'distance_passes_completed',
    'passes_completed_short', 'passes_attempted_short', 'passes_intercepted_by_opp', 'passes_blocked_by_opp',
    'perc_passes_completed_short', 'passes_completed_medium', 'passes_attempted_medium',
    'perc_passes_completed_medium', 'passes_completed_long', 'passes_attempted_long', 'perc_passes_completed_long',
    'passes_to_box', 'crosses', 'assists', 'shots_assists', 'passes_live', 'passes_dead', 'free_kicks',
    'under_pressure_passes', 'switch_passes', 'corner_kicks', 'ground_passes', 'low_passes', 'high_passes',
    'offsides', 'passes_out', 'cut_back_passes', 'through_ball_passes',
    'xg', 'npxg', 'xa',
    'miscontrol',
    'carries', 'distance_carries', 'carries_z1', 'carries_z2', 'carries_z3', 'carries_z4', 'carries_z3_to_z4',
    'carries_att_box', 'lost_by_tackle',
    'blocks', 'shot_blocks', 'target_shot_blocks', 'pass_blocks',
    'touches', 'touches_def_box', 'touches_z1', 'touches_z2', 'touches_z3', 'touches_z4', 'touches_att_box',
    'touches_live',
    'target_of_pass', 'pass_receivings', 'perc_pass_receivings'

    # TODO: progressive
    # 'progressive_passes', 'distance_progressive_passes',
    # 'progressive_carries', 'distance_progressive_carries',
    # 'progressive_pass_receivings',

    # TODO: pressures
    # 'pressures', 'pressures_success', 'perc_pressure_success', '', 'pressures_success_z1',
    # 'perc_pressure_success_z1', '', 'pressures_success_z2', 'perc_pressure_success_z2', '',
    # 'pressures_success_z3', 'perc_pressure_success_z3' '', 'pressures_success_z4',
    # 'perc_pressure_success_z4'

    # TODO: faltan los valores del portero (ver FBREF): goles en contra, paradas, etc.
]

# TODO:duda = ['SCA', 'GCA']

if DEBUG:
    df_metrics_90_min = pd.read_csv('df_metrics_90_min.csv', index_col='team')
else:
    competitions = sb.competitions()
    competition = competitions[(competitions.competition_name == 'La Liga') &
                               (competitions.season_name == '2019/2020')]
    matches = sb.matches(competition_id=int(competition.competition_id),
                         season_id=int(competition.season_id))

    df_team_metrics = None
    for match in matches.iterrows():
        match_events = sb.events(match_id=int(match[1]['match_id']))
        metrics = Metrics(match_events)
        if df_team_metrics is not None:
            df_team_metrics = df_team_metrics.append(metrics.create_aggregations(), ignore_index=True)
        else:
            df_team_metrics = metrics.create_aggregations()
        ic('Match {} processed'.format(match[1]['match_id']))

    df_metrics_90_min = df_team_metrics.groupby('team').mean()

min_max_scaler = MinMaxScaler()

df_metrics_90_min[:] = min_max_scaler.fit_transform(df_metrics_90_min)

n_components = 18

my_model = PCA(n_components=n_components)
my_model.fit(df_metrics_90_min)

# PARA SABER CUÁNTAS COMPONENTES EXPLICAN NUESTRO MODELO DE 120 VARIABLES
# pca = PCA().fit(df_metrics_90_min)
# plt.plot(np.cumsum(pca.explained_variance_ratio_))
# plt.xlabel('number of components')
# plt.ylabel('cumulative explained variance')
# plt.show()

columns = ['pca_%i' % i for i in range(n_components)]
df_pca = pd.DataFrame(my_model.transform(df_metrics_90_min), columns=columns, index=df_metrics_90_min.index)

# nc = range(1, 15)  # El número de iteraciones que queremos hacer.
# kmeans = [KMeans(n_clusters=i) for i in nc]
# score = [kmeans[i].fit(df_pca).score(df_pca) for i in range(len(kmeans))]
#
# plt.xlabel('Número de clústeres (k)')
# plt.ylabel('Suma de los errores cuadráticos')
# plt.plot(nc, score)
# plt.show()

#%% Aplicación de k-means con k = 5.
kmeans = KMeans(n_clusters=6).fit(df_pca)
centroids = kmeans.cluster_centers_

#%% Etiquetamos nuestro dataframe.
labels = kmeans.predict(df_pca)
df_pca['label'] = labels
df_pca = df_pca.sort_values('label')

# X = df_pca.drop(columns='label')
# Y = df_pca['label']

# X_embedded_iso = Isomap(n_components=2).fit_transform(X)
#
# colores = ['red', 'green', 'blue', 'yellow', 'fuchsia', 'orange']
# asignar = []
# for row in labels:
#     asignar.append(colores[row])
# plt.figure(figsize=(12, 5), facecolor='w')
# plt.scatter(X_embedded_iso[:,0], X_embedded_iso[:, 1], c=asignar, s=20)
# plt.text(x=X_embedded_iso[:,0], y=X_embedded_iso[:, 1], s=Y[:])
# plt.title('2D embedding using Isomap \n The color of the points is the price')
# plt.xlabel('Feature 1')
# plt.ylabel('Feature 2')
# plt.tight_layout()
# plt.show()

ic('Aplicar PCA')





