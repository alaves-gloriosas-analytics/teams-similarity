import pandas as pd
import numpy as np
from icecream import ic

ic.configureOutput(includeContext=True)

YARD = 1.09361


class Metrics:
    def __init__(self, events):
        self.events = events
        self.events = self.events.assign(
            location_x=self.events.location.str[0].astype('float'),
            location_y=self.events.location.str[1].astype('float'),
            pass_end_location_x=self.events.pass_end_location.str[0].astype('float'),
            pass_end_location_y=self.events.pass_end_location.str[1].astype('float'),
            carry_end_location_x=self.events.carry_end_location.str[0].astype('float'),
            carry_end_location_y=self.events.carry_end_location.str[1].astype('float')
        )

        self.events.sort_values(['period', 'timestamp'], ascending=[True, True], inplace=True)
        self.events.index = list(np.arange(len(self.events)))
        self.events['carry_distance'] = np.sqrt(((self.events['location_x'] -
                                                  self.events['carry_end_location_x'])**2) +
                                                ((self.events['location_y'] - self.events['carry_end_location_y'])**2))
        self.events['pass_distance'] = np.sqrt(((self.events['location_x'] - self.events['pass_end_location_x'])**2) +
                                                ((self.events['location_y'] - self.events['pass_end_location_y'])**2))
        self.events['total_seconds'] = self.events['minute'].astype('int') * 60 + self.events['second'].astype('int')

    def create_defensive_metrics(self, team):
        ic('Creating defensive metrics')
        team_events = self.events[self.events.team == team]
        column_names = self.events.columns.values.tolist()
        defensive_metrics = {}

        if 'foul_committed_penalty' in column_names:
            penalty_conceded = len(
                team_events[
                    (team_events.type == 'Foul Committed') &
                    (team_events.foul_committed_penalty == True)
                    ]
            )
        else:
            penalty_conceded = 0
        defensive_metrics['penalty_conceded'] = penalty_conceded

        if 'foul_committed_card' in column_names:
            foul_yellow_cards = len(
                team_events[
                    (team_events.type == 'Foul Committed') &
                    (team_events.foul_committed_card == 'Yellow Card')
                    ]
            )
            foul_second_yellow_cards = len(
                team_events[
                    (team_events.type == 'Foul Committed') &
                    (team_events.foul_committed_card == 'Second Yellow')
                    ]
            )
            foul_red_cards = len(
                team_events[
                    (team_events.type == 'Foul Committed') &
                    (team_events.foul_committed_card == 'Red Card')
                    ]
            )
        else:
            foul_yellow_cards = 0
            foul_second_yellow_cards = 0
            foul_red_cards = 0
        if 'bad_behaviour_card' in column_names:
            bad_yellow_cards = len(
                team_events[
                    (team_events.type == 'Bad Behaviour') &
                    (team_events.bad_behaviour_card == 'Yellow Card')
                    ]
            )
            bad_second_yellow_cards = len(
                team_events[
                    (team_events.type == 'Bad Behaviour') &
                    (team_events.bad_behaviour_card == 'Second Yellow')
                    ]
            )
            bad_red_cards = len(
                team_events[
                    (team_events.type == 'Bad Behaviour') &
                    (team_events.bad_behaviour_card == 'Red Card')
                    ]
            )
        else:
            bad_yellow_cards = 0
            bad_second_yellow_cards = 0
            bad_red_cards = 0
        defensive_metrics['yellow_cards'] = bad_yellow_cards + foul_yellow_cards
        defensive_metrics['second_yellow_cards'] = bad_second_yellow_cards + foul_second_yellow_cards
        defensive_metrics['red_cards'] = bad_red_cards + foul_red_cards

        interceptions = len(
            team_events[
                (team_events.type == 'Interception')
            ]
        )
        defensive_metrics['interceptions'] = interceptions

        if 'interception_outcome' in column_names:
            interceptions_ok = len(
                team_events[
                    (team_events.interception_outcome == 'Won') |
                    (team_events.interception_outcome == 'Success') |
                    (team_events.interception_outcome == 'Success In Play') |
                    (team_events.interception_outcome == 'Success Out')
                    ]
            )
        else:
            interceptions_ok = 0
        defensive_metrics['interceptions_ok'] = interceptions_ok

        defensive_metrics['perc_interceptions_ok'] = (interceptions_ok / interceptions) if interceptions != 0 else 0

        clearances = len(
            team_events[
                (team_events.type == 'Clearance')
            ]
        )
        defensive_metrics['clearances'] = clearances

        if 'clearance_aerial_won' in column_names:
            aerial_clearances = len(
                team_events[
                    (team_events.clearance_aerial_won == True)
                ]
            )
        else:
            aerial_clearances = 0
        defensive_metrics['aerial_clearances'] = aerial_clearances

        tackles = len(
            team_events[
                (team_events.duel_type == 'Tackle')
            ]
        )
        defensive_metrics['tackles'] = tackles

        tackles_won = len(
            team_events[
                (team_events.duel_outcome == 'Won') |
                (team_events.duel_outcome == 'Success') |
                (team_events.duel_outcome == 'Success In Play') |
                (team_events.duel_outcome == 'Success Out')
                ]
        )
        defensive_metrics['tackles_won'] = tackles_won

        perc_tackles_won = (tackles_won / tackles) if tackles != 0 else 0
        defensive_metrics['perc_tackles_won'] = perc_tackles_won

        tackles_z1 = len(
            team_events[
                (team_events.duel_type == 'Tackle') &
                (team_events.location_x <= 30)
                ]
        )
        defensive_metrics['tackles_z1'] = tackles_z1

        tackles_z2 = len(
            team_events[
                (team_events.duel_type == 'Tackle') &
                (team_events.location_x > 30) &
                (team_events.location_x <= 60)
                ]
        )
        defensive_metrics['tackles_z2'] = tackles_z2

        tackles_z3 = len(
            team_events[
                (team_events.duel_type == 'Tackle') &
                (team_events.location_x > 60) &
                (team_events.location_x <= 90)
                ]
        )
        defensive_metrics['tackles_z3'] = tackles_z3

        tackles_z4 = len(
            team_events[
                (team_events.duel_type == 'Tackle') &
                (team_events.location_x > 90)
                ]
        )
        defensive_metrics['tackles_z4'] = tackles_z4

        defensive_metrics['interceptions_tackles'] = interceptions + tackles

        fouls_commited = len(
            team_events[
                (team_events.type == 'Foul Committed')
            ]
        )
        defensive_metrics['fouls_commited'] = fouls_commited

        pressures = len(
            team_events[
                (team_events.type == 'Pressure')
            ]
        )
        defensive_metrics['pressures'] = pressures

        pressures_z1 = len(
            team_events[
                (team_events.type == 'Pressure') &
                (team_events.location_x <= 30)
                ]
        )
        defensive_metrics['pressures_z1'] = pressures_z1

        pressures_z2 = len(
            team_events[
                (team_events.type == 'Pressure') &
                (team_events.location_x > 30) &
                (team_events.location_x <= 60)
                ]
        )
        defensive_metrics['pressures_z2'] = pressures_z2

        pressures_z3 = len(
            team_events[
                (team_events.type == 'Pressure') &
                (team_events.location_x > 60) &
                (team_events.location_x <= 90)
                ]
        )
        defensive_metrics['pressures_z3'] = pressures_z3

        pressures_z4 = len(
            team_events[
                (team_events.type == 'Pressure') &
                (team_events.location_x > 90)
                ]
        )
        defensive_metrics['pressures_z4'] = pressures_z4

        blocks_df = (
            team_events[
                (team_events.type == 'Block') &
                (team_events.related_events.notnull())
                ]
        )
        defensive_metrics['blocks'] = len(blocks_df)

        pass_blocks = 0
        shot_blocks = 0
        for block in blocks_df.iterrows():
            if self.events[self.events.id == block[1]['related_events'][0]].type.values[0] == 'Pass':
                pass_blocks += 1
            elif self.events[self.events.id == block[1]['related_events'][0]].type.values[0] == 'Shot':
                shot_blocks += 1

        if 'block_save_block' in column_names:
            target_shot_blocks = len(
                team_events[
                    (team_events.type == 'Block') &
                    (team_events.block_save_block == True)
                    ]
            )
        else:
            target_shot_blocks = 0
        defensive_metrics['target_shot_blocks'] = target_shot_blocks

        return defensive_metrics

    def create_passing_metrics(self, team):
        ic('Creating passing metrics')
        team_events = self.events[self.events.team == team]
        column_names = self.events.columns.values.tolist()
        passing_metrics = {}

        passes_completed_df = (
            team_events[
                (team_events.type == 'Pass') &
                (team_events.pass_outcome.isnull())
                ]
        )

        passes_completed = len(passes_completed_df)
        passing_metrics['passes_completed'] = passes_completed

        passes_attempted = len(
            team_events[
                (team_events.type == 'Pass')
            ]
        )
        passing_metrics['passes_attempted'] = passes_attempted

        passing_metrics['perc_passes_completed'] = (passes_completed / passes_attempted) if passes_attempted > 0 else 0

        passes_completed_z1 = len(
            passes_completed_df[
                (passes_completed_df.location_x <= 30)
            ]
        )
        passing_metrics['passes_completed_z1'] = passes_completed_z1

        passes_attempted_z1 = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.location_x <= 30)
                ]
        )
        passing_metrics['passes_attempted_z1'] = passes_attempted_z1

        passing_metrics['perc_passes_completed_z1'] = (passes_completed_z1 / passes_attempted_z1) if \
            passes_attempted_z1 > 0 else 0

        passes_completed_z2 = len(
            passes_completed_df[
                (passes_completed_df.location_x > 30) &
                (passes_completed_df.location_x <= 60)
                ]
        )
        passing_metrics['passes_completed_z2'] = passes_completed_z2

        passes_attempted_z2 = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.location_x > 30) &
                (team_events.location_x <= 60)
                ]
        )
        passing_metrics['passes_attempted_z2'] = passes_attempted_z2

        passing_metrics['perc_passes_completed_z2'] = (passes_completed_z2 / passes_attempted_z2) if \
            passes_attempted_z2 > 0 else 0

        passes_completed_z3 = len(
            passes_completed_df[
                (passes_completed_df.location_x > 60) &
                (passes_completed_df.location_x <= 90)
                ]
        )
        passing_metrics['passes_completed_z3'] = passes_completed_z3

        passes_attempted_z3 = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.location_x > 60) &
                (team_events.location_x <= 90)
                ]
        )
        passing_metrics['passes_attempted_z3'] = passes_attempted_z3

        passing_metrics['perc_passes_completed_z3'] = (passes_completed_z3 / passes_attempted_z3) \
            if passes_attempted_z3 > 0 else 0

        passes_completed_z4 = len(
            passes_completed_df[
                (passes_completed_df.location_x > 90)
            ]
        )
        passing_metrics['passes_completed_z4'] = passes_completed_z4

        passes_attempted_z4 = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.location_x > 90)
                ]
        )
        passing_metrics['passes_attempted_z4'] = passes_attempted_z4

        passing_metrics['perc_passes_completed_z4'] = (passes_completed_z4 / passes_attempted_z4) if \
            passes_attempted_z4 > 0 else 0

        passes_completed_short = len(
            passes_completed_df[
                (passes_completed_df.pass_length >= 5) &
                (passes_completed_df.pass_length < 15)
                ]
        )
        passing_metrics['passes_completed_short'] = passes_completed_short

        passes_attempted_short = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.pass_length >= 5) &
                (team_events.pass_length < 15)
                ]
        )
        passing_metrics['passes_attempted_short'] = passes_attempted_short

        passing_metrics['perc_passes_completed_short'] = (passes_completed_short / passes_attempted_short) if \
            passes_attempted_short != 0 else 0

        passes_completed_medium = len(
            passes_completed_df[
                (passes_completed_df.pass_length >= 15) &
                (passes_completed_df.pass_length < 30)
                ]
        )
        passing_metrics['passes_completed_medium'] = passes_completed_medium

        passes_attempted_medium = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.pass_length >= 15) &
                (team_events.pass_length < 30)
                ]
        )
        passing_metrics['passes_attempted_medium'] = passes_attempted_medium

        passing_metrics['perc_passes_completed_medium'] = (passes_completed_medium / passes_attempted_medium) if \
            passes_attempted_medium != 0 else 0

        passes_completed_long = len(
            passes_completed_df[
                (passes_completed_df.pass_length >= 30)
            ]
        )
        passing_metrics['passes_completed_long'] = passes_completed_long

        passes_attempted_long = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.pass_length >= 30)
                ]
        )
        passing_metrics['passes_attempted_long'] = passes_attempted_long

        passing_metrics['perc_passes_completed_long'] = (passes_completed_long / passes_attempted_long) if \
            passes_attempted_long != 0 else 0

        passes_to_box = len(
            passes_completed_df[
                (passes_completed_df.pass_type != 'Corner') &
                (passes_completed_df.pass_type != 'Free Kick') &
                (passes_completed_df.pass_type != 'Goal Kick') &
                (passes_completed_df.pass_type != 'Throw-in') &
                (passes_completed_df.pass_type != 'Kick Off') &
                ((passes_completed_df.location_x < 102) |
                 (passes_completed_df.location_y < 18) |
                 (passes_completed_df.location_y > 62)) &
                (passes_completed_df.pass_end_location_x > 102) &
                (passes_completed_df.pass_end_location_y > 18) &
                (passes_completed_df.pass_end_location_y < 62)
                ]
        )
        passing_metrics['passes_to_box'] = passes_to_box

        crosses = len(
            passes_completed_df[
                (passes_completed_df.pass_cross == True)
            ]
        )
        passing_metrics['crosses'] = crosses

        if 'pass_goal_assist' in column_names:
            assists = len(
                team_events[
                    (team_events.pass_goal_assist == True)
                ]
            )
        else:
            assists = 0
        passing_metrics['assists'] = assists

        shots_assists = len(
            team_events[
                (team_events.pass_shot_assist == True)
            ]
        )
        passing_metrics['shots_assists'] = shots_assists

        passes_live = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.pass_type != 'Corner') &
                (team_events.pass_type != 'Free Kick') &
                (team_events.pass_type != 'Goal Kick') &
                (team_events.pass_type != 'Throw-in') &
                (team_events.pass_type != 'Kick Off')
                ]
        )
        passing_metrics['passes_live'] = passes_live

        passes_dead = len(
            team_events[
                (team_events.type == 'Pass') &
                ((team_events.pass_type == 'Corner') |
                 (team_events.pass_type == 'Free Kick') |
                 (team_events.pass_type == 'Goal Kick') |
                 (team_events.pass_type == 'Throw-in') |
                 (team_events.pass_type == 'Kick Off'))
                ]
        )
        passing_metrics['passes_dead'] = passes_dead

        free_kicks = len(
            team_events[
                (team_events.pass_type == 'Free Kick')
            ]
        )
        passing_metrics['free_kicks'] = free_kicks

        corner_kicks = len(
            team_events[
                (team_events.pass_type == 'Corner')
            ]
        )
        passing_metrics['corner_kicks'] = corner_kicks

        ground_passes = len(
            team_events[
                (team_events.pass_height == 'Ground Pass')
            ]
        )
        passing_metrics['ground_passes'] = ground_passes

        low_passes = len(
            team_events[
                (team_events.pass_height == 'Low Pass')
            ]
        )
        passing_metrics['low_passes'] = low_passes

        high_passes = len(
            team_events[
                (team_events.pass_height == 'High Pass')
            ]
        )
        passing_metrics['high_passes'] = high_passes

        offsides = len(
            team_events[
                (team_events.pass_outcome == 'Pass Offside')
            ]
        )
        passing_metrics['offsides'] = offsides

        passes_out = len(
            team_events[
                (team_events.pass_outcome == 'Out')
            ]
        )
        passing_metrics['passes_out'] = passes_out

        if 'pass_cut_back' in column_names:
            cut_back_passes = len(
                team_events[
                    (team_events.pass_cut_back == True)
                ]
            )
        else:
            cut_back_passes = 0
        passing_metrics['cut_back_passes'] = cut_back_passes

        if 'pass_switch' in column_names:
            switch_passes = len(
                team_events[
                    (team_events.pass_switch == True)
                ]
            )
        else:
            switch_passes = 0
        passing_metrics['switch_passes'] = switch_passes

        through_ball_passes = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.pass_technique == 'Through Ball')
                ]
        )
        passing_metrics['through_ball_passes'] = through_ball_passes

        under_pressure_passes = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.under_pressure == True)
                ]
        )
        passing_metrics['under_pressure_passes'] = under_pressure_passes

        passing_metrics['distance_passes_completed'] = int(passes_completed_df['pass_length'].sum())

        target_of_pass = len(
            team_events[
                (team_events.type == 'Pass') &
                (team_events.pass_recipient.notnull())
                ]
        )
        passing_metrics['target_of_pass'] = target_of_pass

        passing_metrics['pass_receivings'] = passes_completed

        passing_metrics['perc_pass_receivings'] = (passes_completed / target_of_pass) if target_of_pass != 0 else 0

        return passing_metrics

    def create_attacking_metrics(self, team):
        ic('Creating attacking metrics')
        team_events = self.events[self.events.team == team]
        column_names = self.events.columns.values.tolist()
        attacking_metrics = {}

        if 'shot_outcome' in column_names:
            goals = len(team_events[team_events.shot_outcome == 'Goal'])
        else:
            goals = 0
        attacking_metrics['goals'] = goals

        penalty_attempted = len(
            team_events[
                (team_events.type == 'Shot') &
                (team_events.shot_type == 'Penalty')
                ]
        )
        attacking_metrics['penalty_attempted'] = penalty_attempted

        penalty_goal = len(
            team_events[
                (team_events.type == 'Shot') &
                (team_events.shot_type == 'Penalty') &
                (team_events.shot_outcome == 'Goal')
                ]
        )
        attacking_metrics['penalty_goal'] = penalty_goal

        shots = len(
            team_events[
                (team_events.type == 'Shot') &
                (team_events.shot_type != 'Penalty')
                ]
        )
        attacking_metrics['shots'] = shots

        shots_out_box = len(
            team_events[
                (team_events.type == 'Shot') &
                (team_events.shot_type != 'Penalty') &
                ((team_events.location_x < 102) |
                 ((team_events.location_x >= 102) &
                  ((team_events.location_y < 18) |
                   (team_events.location_y > 62))))
                ]
        )
        attacking_metrics['shots_out_box'] = shots_out_box

        shots_in_box = len(
            team_events[
                (team_events.type == 'Shot') &
                (team_events.shot_type != 'Penalty') &
                (team_events.location_x >= 102) &
                (team_events.location_y >= 18) &
                (team_events.location_y <= 62)
                ]
        )
        attacking_metrics['shots_in_box'] = shots_in_box

        shots_on_target = len(
            team_events[
                (team_events.type == 'Shot') &
                (team_events.shot_type != 'Penalty') &
                ((team_events.shot_outcome == 'Saved') |
                 (team_events.shot_outcome == 'Saved To Post') |
                 (team_events.shot_outcome == 'Goal'))
                ]
        )
        attacking_metrics['shots_on_target'] = shots_on_target

        dribbles_completed = len(
            team_events[
                (team_events.dribble_outcome == 'Complete')
            ]
        )
        attacking_metrics['dribbles_completed'] = dribbles_completed

        dribbles_attempted = len(
            team_events[
                (team_events.type == 'Dribble')
            ]
        )
        attacking_metrics['dribbles_attempted'] = dribbles_attempted

        attacking_metrics['perc_dribbles_completed'] = (dribbles_completed / dribbles_attempted) if dribbles_attempted > 0 \
            else 0

        dribbles_completed_z1 = len(
            team_events[
                (team_events.dribble_outcome == 'Complete') &
                (team_events.location_x <= 30)
                ]
        )
        attacking_metrics['dribbles_completed_z1'] = dribbles_completed_z1

        dribbles_attempted_z1 = len(
            team_events[
                (team_events.type == 'Dribble') &
                (team_events.location_x <= 30)
                ]
        )
        attacking_metrics['dribbles_attempted_z1'] = dribbles_attempted_z1

        attacking_metrics['perc_dribbles_completed_z1'] = (dribbles_completed_z1 / dribbles_attempted_z1) if \
            dribbles_attempted_z1 > 0 else 0

        dribbles_completed_z2 = len(
            team_events[
                (team_events.dribble_outcome == 'Complete') &
                (team_events.location_x > 30) &
                (team_events.location_x <= 60)
                ]
        )
        attacking_metrics['dribbles_completed_z2'] = dribbles_completed_z2

        dribbles_attempted_z2 = len(
            team_events[
                (team_events.type == 'Dribble') &
                (team_events.location_x > 30) &
                (team_events.location_x <= 60)
                ]
        )
        attacking_metrics['dribbles_attempted_z2'] = dribbles_attempted_z2

        attacking_metrics['perc_dribbles_completed_z2'] = (dribbles_completed_z2 / dribbles_attempted_z2) if \
            dribbles_attempted_z2 > 0 else 0

        dribbles_completed_z3 = len(
            team_events[
                (team_events.dribble_outcome == 'Complete') &
                (team_events.location_x > 60) &
                (team_events.location_x <= 90)
                ]
        )
        attacking_metrics['dribbles_completed_z3'] = dribbles_completed_z3

        dribbles_attempted_z3 = len(
            team_events[
                (team_events.type == 'Dribble') &
                (team_events.location_x > 60) &
                (team_events.location_x <= 90)
                ]
        )
        attacking_metrics['dribbles_attempted_z3'] = dribbles_attempted_z3

        attacking_metrics['perc_dribbles_completed_z3'] = (dribbles_completed_z3 / dribbles_attempted_z3) if \
            dribbles_attempted_z3 > 0 else 0

        dribbles_completed_z4 = len(
            team_events[
                (team_events.dribble_outcome == 'Complete') &
                (team_events.location_x > 90)
                ]
        )
        attacking_metrics['dribbles_completed_z4'] = dribbles_completed_z4

        dribbles_attempted_z4 = len(
            team_events[
                (team_events.type == 'Dribble') &
                (team_events.location_x > 90)
                ]
        )
        attacking_metrics['dribbles_atempted_z4'] = dribbles_attempted_z4

        attacking_metrics['perc_dribbles_completed_z4'] = (dribbles_completed_z4 / dribbles_attempted_z4) if \
            dribbles_attempted_z4 > 0 else 0

        carries_df = (
            team_events[
                (team_events.carry_end_location.notnull()) &
                (team_events.location.notnull())
                ]
        )

        attacking_metrics['carries'] = len(carries_df)

        attacking_metrics['distance_carries'] = int(carries_df['carry_distance'].sum())

        carries_z1 = len(
            carries_df[
                (carries_df.carry_end_location_x <= 30)
            ]
        )
        attacking_metrics['carries_z1'] = carries_z1

        carries_z2 = len(
            carries_df[
                (carries_df.carry_end_location_x > 30) &
                (carries_df.carry_end_location_x <= 60)
                ]
        )
        attacking_metrics['carries_z2'] = carries_z2

        carries_z3 = len(
            carries_df[
                (carries_df.carry_end_location_x > 60) &
                (carries_df.carry_end_location_x <= 90)
                ]
        )
        attacking_metrics['carries_z3'] = carries_z3

        carries_z4 = len(
            carries_df[
                (carries_df.carry_end_location_x > 90) &
                (carries_df.carry_distance > 1)
                ]
        )
        attacking_metrics['carries_z4'] = carries_z4

        carries_z3_to_z4 = len(
            carries_df[
                (carries_df.location.notnull()) &
                (carries_df.location_x < 90) &
                (carries_df.carry_end_location.notnull()) &
                (carries_df.carry_end_location_x >= 90) &
                (carries_df.carry_distance > 0)
                ]
        )
        attacking_metrics['carries_z3_to_z4'] = carries_z3_to_z4

        carries_att_box = len(
            carries_df[
                ((carries_df.location_x < 102) |
                 (carries_df.location_y < 18) |
                 (carries_df.location_y > 62)) &
                (carries_df.carry_end_location_x > 102) &
                (carries_df.carry_end_location_y > 18) &
                (carries_df.carry_end_location_y < 62)
                ]
        )
        attacking_metrics['carries_att_box'] = carries_att_box

        touches_df = (
            team_events[
                ((team_events.type == 'Pass') |
                 (team_events.type == 'Ball Receipt*') |
                 (team_events.type == 'Shot') |
                 (team_events.type == 'Carry') |
                 (team_events.type == 'Dribble')) &
                (team_events.possession_team == team)
                ]
        )

        possesions_dict = {k: v for k, v in touches_df.groupby('possession')}

        touches_def_box = 0
        touches_att_box = 0
        touches = 0
        touches_z1 = 0
        touches_z2 = 0
        touches_z3 = 0
        touches_z4 = 0
        touches_live = 0
        for key in possesions_dict:
            possesions_dict[key]['cumsum'] = (possesions_dict[key]["player"] != possesions_dict[key]["player"]
                                              .shift()).cumsum()

            touches += int(possesions_dict[key]['cumsum'].max())

            possesions_touches_df = possesions_dict[key].groupby('cumsum').first()
            touches_def_box += len(
                possesions_touches_df[
                    (possesions_touches_df.location_x <= 18) &
                    (possesions_touches_df.location_y >= 18) &
                    (possesions_touches_df.location_y <= 62)
                    ]
            )

            touches_att_box += len(
                possesions_touches_df[
                    (possesions_touches_df.location_x >= 102) &
                    (possesions_touches_df.location_y >= 18) &
                    (possesions_touches_df.location_y <= 62)
                    ]
            )

            touches_z1 += len(
                possesions_touches_df[
                    (possesions_touches_df.location_x <= 30)
                ]
            )

            touches_z2 += len(
                possesions_touches_df[
                    (possesions_touches_df.location_x > 30) &
                    (possesions_touches_df.location_x <= 60)
                    ]
            )

            touches_z3 += len(
                possesions_touches_df[
                    (possesions_touches_df.location_x > 60) &
                    (possesions_touches_df.location_x <= 90)
                    ]
            )

            touches_z4 += len(
                possesions_touches_df[
                    (possesions_touches_df.location_x > 90)
                ]
            )

            dead_actions = ['Corner', 'Free Kick', 'Goal Kick', 'Kick Off', 'Throw In', 'Penalty']
            dead_action_detected = False
            for dead_action in dead_actions:
                # Check if the element exists in dataframe values
                if dead_action in possesions_dict[key]['pass_type'].values or \
                        dead_action in possesions_dict[key]['shot_type'].values:
                    dead_action_detected = True
                    break

            if not dead_action_detected:
                touches_live += int(possesions_dict[key]['cumsum'].max())

        attacking_metrics['touches'] = touches
        attacking_metrics['touches_live'] = touches_live
        attacking_metrics['touches_def_box'] = touches_def_box
        attacking_metrics['touches_att_box'] = touches_att_box
        attacking_metrics['touches_z1'] = touches_z1
        attacking_metrics['touches_z2'] = touches_z2
        attacking_metrics['touches_z3'] = touches_z3
        attacking_metrics['touches_z4'] = touches_z4

        return attacking_metrics

    def create_other_metrics(self, team):
        ic('Creating attacking metrics')
        team_events = self.events[self.events.team == team]
        column_names = self.events.columns.values.tolist()
        other_metrics = {}

        # TODO: Revisar
        lost_ball_recoveries = len(
            team_events[
                (team_events.type == 'Ball Recovery')
            ]
        )
        other_metrics['lost_ball_recoveries'] = lost_ball_recoveries

        if 'clearance_aerial_won' in column_names:
            clearance_aerial_duels_won = len(
                team_events[
                    (team_events.clearance_aerial_won == True)
                ]
            )
        else:
            clearance_aerial_duels_won = 0

        if 'miscontrol_aerial_won' in column_names:
            miscontrol_aerial_duels_won = len(
                team_events[
                    (team_events.miscontrol_aerial_won == True)
                ]
            )
        else:
            miscontrol_aerial_duels_won = 0

        if 'shot_aerial_won' in column_names:
            shot_aerial_duels_won = len(
                team_events[
                    (team_events.shot_aerial_won == True)
                ]
            )
        else:
            shot_aerial_duels_won = 0
        if 'pass_aerial_won' in column_names:
            pass_aerial_duels_won = len(
                team_events[
                    (team_events.pass_aerial_won == True)
                ]
            )
        else:
            pass_aerial_duels_won = 0

        aerial_duels_won = clearance_aerial_duels_won + miscontrol_aerial_duels_won + \
                           shot_aerial_duels_won + pass_aerial_duels_won
        other_metrics['aerial_duels_won'] = aerial_duels_won

        fouls_received = len(
            team_events[
                (team_events.type == 'Foul Won')
            ]
        )
        other_metrics['fouls_received'] = fouls_received

        aerial_duels_lost = len(
            team_events[
                (team_events.duel_type == 'Aerial Lost')
            ]
        )
        other_metrics['aerial_duels_lost'] = aerial_duels_lost

        other_metrics['perc_aerial_duels_won'] = (aerial_duels_won / (aerial_duels_lost + aerial_duels_won)) if \
            (aerial_duels_lost + aerial_duels_won) != 0 else 0

        other_metrics['xg'] = team_events.shot_statsbomb_xg.sum(skipna=True)

        other_metrics['npxg'] = (
            team_events[
                (team_events.shot_statsbomb_xg.notnull()) &
                (team_events.shot_type != 'Penalty')
                ]

        ).shot_statsbomb_xg.sum()

        if 'pass_goal_assist' in column_names:
            shots_assists_df = (
                team_events[
                    (team_events.pass_shot_assist == True) |
                    (team_events.pass_goal_assist == True)
                    ]
            )

            xa = 0
            for shot_assist in shots_assists_df.iterrows():
                xa += team_events[team_events.id == shot_assist[1]['pass_assisted_shot_id']].shot_statsbomb_xg.values[0]
        else:
            xa = 0
        other_metrics['xa'] = xa

        miscontrol = len(
            team_events[
                (team_events.type == 'Miscontrol')
            ]
        )
        other_metrics['miscontrol'] = miscontrol

        opp_blocks_df = (
            self.events[
                (self.events.team != team) &
                (self.events.type == 'Block') &
                (self.events.related_events.notnull())
                ]
        )
        passes_blocked_by_opp = 0
        for opp_block in opp_blocks_df.iterrows():
            if self.events[self.events.id == opp_block[1]['related_events'][0]].type.values[0] == 'Pass':
                passes_blocked_by_opp += 1
        other_metrics['passes_blocked_by_opp'] = passes_blocked_by_opp

        opp_interceptions_df = (
            self.events[
                (self.events.team != team) &
                (self.events.type == 'Interception') &
                (self.events.related_events.notnull())
                ]
        )
        passes_intercepted_by_opp = 0
        for opp_interception in opp_interceptions_df.iterrows():
            if self.events[self.events.id == opp_interception[1]['related_events'][0]].type.values[0] == 'Pass':
                passes_intercepted_by_opp += 1
        other_metrics['passes_intercepted_by_opp'] = passes_intercepted_by_opp

        opp_tackles_df = (
            self.events[
                (self.events.team != team) &
                (self.events.duel_type == 'Tackle') &
                # (events.related_events.notnull())
                ((self.events.duel_outcome == 'Won') |
                 (self.events.duel_outcome == 'Success') |
                 (self.events.duel_outcome == 'Success In Play') |
                 (self.events.duel_outcome == 'Lost In Play') |
                 (self.events.duel_outcome == 'Lost Out') |
                 (self.events.duel_outcome == 'Success Out'))
                ]
        )
        lost_by_tackle = 0
        for opp_tackle in opp_tackles_df.iterrows():
            if self.events[self.events.id == opp_tackle[1]['related_events'][0]].type.values[0] != 'Dribble':
                lost_by_tackle += 1
        other_metrics['lost_by_tackle'] = lost_by_tackle

        return other_metrics

    def create_aggregations(self):
        df_metrics = None
        teams = self.events.team.unique()

        for team in teams:
            team_aggr = {'team': team}
            team_aggr.update(self.create_defensive_metrics(team))
            team_aggr.update(self.create_attacking_metrics(team))
            team_aggr.update(self.create_passing_metrics(team))
            team_aggr.update(self.create_other_metrics(team))

            if df_metrics is not None:
                df_metrics = df_metrics.append(team_aggr, ignore_index=True)
            else:
                df_metrics = pd.DataFrame(team_aggr, index=[0])

        return df_metrics

            # TODO: pruebas
            # success_pressures = 0
            # time_counter = None
            # player_name = None
            # pressure_counter = 0
            # seconds = 0
            # for event in events.iterrows():
            #     if time_counter is not None and event[1]['total_seconds'] <= (time_counter + 5) and event[1]['type'] != 'Pressure':
            #         # print(pressure_counter, event[1]['possession_team'])
            #         if event[1]['type'] == 'Shot':
            #             time_counter = None
            #         elif event[1]['possession_team'] == team:
            #             success_pressures += 1
            #             time_counter = None
            #             print(player_name)
            #     elif event[1]['type'] == 'Pressure' and event[1]['team'] == team:
            #         time_counter = event[1]['total_seconds']
            #         player_name = event[1]['player']
            #         seconds = event[1]['total_seconds']
            #         pressure_counter += 1

# ', 'pressures_success', 'perc_pressure_success', '', 'pressures_success_z1',
# 'perc_pressure_success_z1', '', 'pressures_success_z2', 'perc_pressure_success_z2', '',
# 'pressures_success_z3', 'perc_pressure_success_z3' '', 'pressures_success_z4',
# 'perc_pressure_success_z4',